# Vagrant Alma Linux 8 Desktop

Installs a Alma Linux 8 (GNOME) Desktop in [libvirt](https://libvirt.org/).

* Configure [Vagrant](https://developer.hashicorp.com/vagrant/tutorials/getting-started)
* git clone this repository in `~/Vagrant` and
* run `vagrant up`
